function FootBall(parentElement, width, height) {
    this.width = width;
    this.height = height;
    this.started = false;
    this.angle = Math.random() * 90 - 45;

    this._render(parentElement);
    this._bindEvents(parentElement.ownerDocument);
}

FootBall.PLAYER_SPEED = 250; // pixels per second
FootBall.BALL_SPEED = 500; // pixels per second

FootBall.prototype._createElement = function (tagName, parentElement, className) {
    var element = parentElement.ownerDocument.createElement(tagName);
    parentElement.appendChild(element);
    if (typeof(className) == 'string') {
        element.className = className;
    }
    return element;
};

FootBall.prototype._render = function (parentElement) {
    this.body = this._createElement('div', parentElement, 'football-field-outer');
    this.body.style.width = this.width + 'px';
    this.body.style.height = this.height + 'px';

    this.field = this._createElement('div', this.body, 'football-field-inner');

    this.ball = this._createElement('div', this.field, 'football-ball');
    this.ball.style.top = (this.field.clientHeight / 2 - this.ball.clientHeight / 2) + 'px';

    this.player = this._createElement('div', this.field, 'football-player');
    this.player.style.top = (this.field.clientHeight / 2 - this.player.clientHeight / 2) + 'px';
};

FootBall.prototype._bindEvents = function (document) {
    var football = this;

    this.keypressed = null;

    document.addEventListener('keydown', function (e) {
        if (!football.keypressed) {
            football.keypressed = true;

            var direction;
            if (e.which == 38) {
                direction = -1;
            }
            if (e.which == 40) {
                direction = +1;
            }
            if (direction) {
                var startTimer = window.performance.now(), startPosition = football.player.offsetTop;

                function step(now) {
                    var progress = (now - startTimer) / 1000;

                    var newPosition = startPosition + (FootBall.PLAYER_SPEED * progress) * direction;

                    newPosition = Math.max(0, Math.min(newPosition, football.field.clientHeight - football.player.clientHeight));

                    football.player.style.top = newPosition + 'px';

                    if (football.keypressed) {
                        window.requestAnimationFrame(step);
                    }
                }

                window.requestAnimationFrame(step);
            }
        }
    });

    document.addEventListener('keyup', function (e) {
        football.keypressed = false;
    });
};

FootBall.prototype.play = function () {
    var football = this, startTimer = window.performance.now();

    function generateNewPosition(progress, startPosition) {
        return {
            top: startPosition.top + Math.sin(Math.PI * football.angle / 180) * (FootBall.BALL_SPEED * progress),
            left: startPosition.left + Math.cos(Math.PI * football.angle / 180) * (FootBall.BALL_SPEED * progress)
        }
    }

    function step(now) {
        var startPosition = {
            top: football.ball.offsetTop,
            left: football.ball.offsetLeft
        };

        var progress = (now - startTimer) / 1000;
        startTimer = now;

        var newPosition = generateNewPosition(progress, startPosition);

        football.ball.style.top = newPosition.top + 'px';
        football.ball.style.left = newPosition.left + 'px';

        football.requestID = window.requestAnimationFrame(step);
    }

    football.requestID = window.requestAnimationFrame(step);
};